use super::models::MinifluxError;
use failure::{Backtrace, Context, Fail};
use std::fmt;

#[derive(Debug)]
pub struct ApiError {
    inner: Context<ApiErrorKind>,
}

#[derive(Clone, Eq, PartialEq, Debug, Fail)]
pub enum ApiErrorKind {
    #[fail(display = "Url Error")]
    Url,
    #[fail(display = "Json Error")]
    Json,
    #[fail(display = "Http request failed")]
    Http,
    #[fail(display = "Miniflux error")]
    Miniflux(MinifluxError),
    #[fail(display = "Request failed with message access denied")]
    AccessDenied,
    #[fail(display = "Error parsing string (enum/date) returned by miniflux")]
    Parse,
    #[fail(display = "Unknown error")]
    Unknown,
}

impl Fail for ApiError {
    fn cause(&self) -> Option<&dyn Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

impl ApiError {
    pub fn kind(&self) -> ApiErrorKind {
        self.inner.get_context().clone()
    }
}

impl From<ApiErrorKind> for ApiError {
    fn from(kind: ApiErrorKind) -> ApiError {
        ApiError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<ApiErrorKind>> for ApiError {
    fn from(inner: Context<ApiErrorKind>) -> ApiError {
        ApiError { inner }
    }
}
