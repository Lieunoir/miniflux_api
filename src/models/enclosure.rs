use crate::{EnclosureID, EntryID, UserID};
use serde_derive::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Enclosure {
    pub id: EnclosureID,
    pub user_id: UserID,
    pub entry_id: EntryID,
    pub url: String,
    pub mime_type: String,
    pub size: i64,
}

impl Enclosure {
    /// destroy this category and gain ownershipt of the data it contains
    pub fn decompose(self) -> (EnclosureID, UserID, EntryID, String, String, i64) {
        (
            self.id,
            self.user_id,
            self.entry_id,
            self.url,
            self.mime_type,
            self.size,
        )
    }
}
