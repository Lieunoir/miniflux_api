use crate::{CategoryID, UserID};
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize)]
pub struct Category {
    pub id: CategoryID,
    pub user_id: UserID,
    pub title: String,
}

impl Category {
    /// destroy this category and gain ownershipt of the data it contains
    pub fn decompose(self) -> (CategoryID, UserID, String) {
        (self.id, self.user_id, self.title)
    }
}

#[derive(Debug, Serialize)]
pub struct CategoryInput {
    pub title: String,
}
